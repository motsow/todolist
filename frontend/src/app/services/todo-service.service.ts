import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class TodoService {
  
  private baseUrl = 'http://127.0.0.1:8000/api';

  constructor(private http: HttpClient) { }

  createTodo(body) {
    return this.http.post(`${this.baseUrl}/todo`, body);
  }

  updateTodo(sid: string, todo) {
    return this.http.put(`${this.baseUrl}/${sid}/todo`, { ...todo });
  }

  deleteTodo(sid: string) {
    return this.http.delete(`${this.baseUrl}/${sid}/todo`);
  }

  getTodos() {
    return this.http.get(`${this.baseUrl}/todo`);
  }
}