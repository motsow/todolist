import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import {AuthService} from '../services/auth.service';
// import {Observable, of} from 'rxjs';
// import {Router} from "@angular/router";
// import {catchError} from "rxjs/internal/operators";

@Injectable({
  providedIn: 'root',
})
export class TodolistService {
  private baseUrl = 'http://localhost:8000/api';

  constructor(private http: HttpClient) { }

  signup(data) {
    return this.http.post(`${this.baseUrl}/signup`, data);
  }

  login(data) {
    return this.http.post(`${this.baseUrl}/login`, data);
  }

  sendPasswordResetLink(data) {
    return this.http.post(`${this.baseUrl}/sendPasswordResetLink`, data);
  }
  
  changePassword(data) {
    return this.http.post(`${this.baseUrl}/resetPassword`, data);
  }

 

}
