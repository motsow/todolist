import { Component, OnInit, Input } from '@angular/core';
import { TodolistService } from '../services/todolist.service';
import { TokenService } from '../services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public form = {
    name: null,
    email: null,
    password: null,
    password_confirmation: null
  };
  public error = [];

  constructor(
    private Todolist: TodolistService,
    private Token: TokenService,
    private router: Router
  ) { }

  onSubmit() {
    this.Todolist.signup(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }
  handleResponse(data) {
    this.Token.handle(data.access_token);
    this.router.navigateByUrl('/todo');
  }

  handleError(error) {
    this.error = error.error.errors;
  }

  ngOnInit() {
  }

}