import { Pipe, PipeTransform } from '@angular/core';
// import { RecipeModel } from '../_shared/model/recipe.model';


@Pipe({
    name: 'todofilter'
})

export class TodoFilterPipe implements PipeTransform {
  transform(todos: any[], searchText: string): any {
    return todos.filter(todo => todo.item.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
}
}
