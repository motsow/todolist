import { Component, OnInit } from '@angular/core';
import { TodolistService } from '../../services/Todolist.service';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-request-reset',
  templateUrl: './request-reset.component.html',
  styleUrls: ['./request-reset.component.css']
})
export class RequestResetComponent implements OnInit {
  public form = {
    email: null
  };


  constructor(
    private Todolist: TodolistService,
    private notify: SnotifyService,
    private Notfiy:SnotifyService
  ) { }

  ngOnInit() {
  }


  onSubmit() {
    this.Notfiy.info('Wait...' ,{timeout:5000})
    this.Todolist.sendPasswordResetLink(this.form).subscribe(
      // data => console.log(data),
      data => this.handleResponse(data),
      // error => console.log(error),
      error => this.notify.error(error.error.error)
    );
  }

  handleResponse(res)  {
    this.Notfiy.success(res.data,{timeout:0});
    this.form.email = null;
  }

}
